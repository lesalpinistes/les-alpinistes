<?php

/**
 *   Les Alpinistes
 *
 * @package Les Alpinistes
 * @author LesAlpinistes <tech@lesalpinistes.com>
 * @version 1.0.0
 * @link https://lesalpinistes.com
 */

// prise en compte du dossier de traduction du theme enfant à la place du parent
function alp_load_languages()
{
    load_child_theme_textdomain('clayton', get_stylesheet_directory() . '/languages');
    // languages étant le chemin du dossier dans lequel se trouvent vos fichiers .po et .mo
}

add_action('after_setup_theme', 'alp_load_languages');

function alp_login_style()
{
    ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo
            get_stylesheet_directory_uri(); ?>/assets/img/lesAlpinistes.svg);
            width: 20rem;
            height: 8rem;
            background-size: 20rem 8rem;
        }
    </style>
    <?php
}

add_action('login_enqueue_scripts', 'alp_login_style');

function alp_design()
{
    wp_enqueue_script(
        'alpine-js',
        get_theme_file_uri('/assets/js/alpine.js'),
        array('jquery')
    );
}

add_action('wp_enqueue_scripts', 'alp_design');


/**
 * Get the posts navigation markup.
 *
 * @param array $config Configuration array.
 * @return string
 * @since 1.0.0
 */
function evolvethemes_get_pagination($config = array())
{
    if ($config === false) {
        return '';
    }

    global $wp_query;

    $config = wp_parse_args($config, array(
        'query' => $wp_query,
        'title' => __('Posts navigation', 'clayton'),
        'range' => 1,
        'prev' => '&lsaquo;',
        'next' => '&rsaquo;',
    ));

    extract($config);

    $allowed_html = array(
        'span' => array(
            'class' => array(),
        ),
    );

    /* Total number of pages. */
    $pages = $query->max_num_pages ? absint($query->max_num_pages) : 1;

    if ($pages <= 1) {
        return '';
    }

    $html = '';

    /* Current page. */
    $paged = 1;

    if (get_query_var('paged')) {
        $paged = absint(get_query_var('paged'));
    } elseif (get_query_var('page')) {
        $paged = absint(get_query_var('page'));
    }

    /* Link back to the first page. */
    $show_first = true;

    /* Link to the last page. */
    $show_last = true;

    /* Link to the next page. */
    $show_next = $paged < $pages;

    /* Link to the previous page. */
    $show_prev = $paged > 1;

    $html .= '<nav class="navigation pagination">';
    if (!empty($config['title'])) {
        $html .= sprintf('<h2 class="screen-reader-text">%s</h2>', esc_html($config['title']));
    }

    $link = '<a class="%s page-numbers" title="%s" href="%s">%s</a>';
    $current = '<span class="%s page-numbers" title="%s" href="%s">%s</span>';

    $html .= '<div class="nav-links">';
    if ($show_next) {
        $html .= sprintf('<a class="next page-numbers" title="%s" href="%s">%s</a>',
            esc_attr(__('Go to the next page', 'clayton')),
            esc_attr(get_pagenum_link($paged + 1)),
            esc_html($next)
        );
    }

    if ($show_first) {
        $show_first_class = '';
        $show_first_html = $link;

        if (1 == $paged) {
            $show_first_class = 'current';
            $show_first_html = $current;
        }

        $html .= sprintf($show_first_html,
            esc_attr($show_first_class),
            esc_attr(__('Go to the first page', 'clayton')),
            esc_attr(get_pagenum_link(1)),
            esc_html(1)
        );
    }

    if ($paged - $range > 2) {
        $html .= '<span class="page-numbers dots">&hellip;</span>';
    }

    for ($i = 2; $i < $pages; $i++) {
        if ($i <= $paged + $range && $i >= $paged - $range) {
            $number_class = '';
            $number_html = $link;

            if ($i == $paged) {
                $number_class = 'current';
                $number_html = $current;
            }

            $text = sprintf(wp_kses(__('<span class="meta-nav screen-reader-text"></span>%s', 'clayton'), $allowed_html), $i);

            $html .= sprintf($number_html,
                esc_attr($number_class),
                esc_attr(sprintf(__('Go to page number %s', 'clayton'), $i)),
                esc_attr(get_pagenum_link($i)),
                wp_kses_post($text),
                esc_html($i)
            );
        }
    }

    if ($paged < $pages - $range - 1) {
        $html .= '<span class="page-numbers dots">&hellip;</span>';
    }

    if ($show_last) {
        $show_last_class = '';
        $show_last_html = $link;

        if ($pages == $paged) {
            $show_last_class = 'current';
            $show_last_html = $current;
        }

        $html .= sprintf($show_last_html,
            esc_attr($show_last_class),
            esc_attr(__('Go to the last page', 'clayton')),
            esc_attr(get_pagenum_link($pages)),
            esc_html($pages)
        );
    }

    if ($show_prev) {
        $html .= sprintf('<a class="prev page-numbers" title="%s" href="%s">%s</a>',
            esc_attr(__('Go to the previous page', 'clayton')),
            esc_attr(get_pagenum_link($paged - 1)),
            esc_html($prev)
        );
    }
    $html .= '</div>';
    $html .= '</nav>';

    return $html;
}

/*-----------------------------------------------------------------------------------*/
/* remove Jetpack CSS file
/*-----------------------------------------------------------------------------------*/
// First, make sure Jetpack doesn't concatenate all its CSS
add_filter('jetpack_implode_frontend_css', '__return_false');

// Then, remove each CSS file, one at a time
function jeherve_remove_all_jp_css()
{
    wp_deregister_style('AtD_style'); // After the Deadline
    wp_deregister_style('jetpack_likes'); // Likes
    //wp_deregister_style( 'jetpack-carousel' ); // Carousel
    wp_deregister_style('grunion.css'); // Grunion contact form
    wp_deregister_style('the-neverending-homepage'); // Infinite Scroll
    wp_deregister_style('infinity-twentyten'); // Infinite Scroll - Twentyten Theme
    wp_deregister_style('infinity-twentyeleven'); // Infinite Scroll - Twentyeleven Theme
    wp_deregister_style('infinity-twentytwelve'); // Infinite Scroll - Twentytwelve Theme
    wp_deregister_style('noticons'); // Notes
    wp_deregister_style('post-by-email'); // Post by Email
    wp_deregister_style('publicize'); // Publicize
    wp_deregister_style('sharedaddy'); // Sharedaddy
    wp_deregister_style('sharing'); // Sharedaddy Sharing
    wp_deregister_style('stats_reports_css'); // Stats
    //wp_deregister_style( 'jetpack-widgets' ); // Widgets
    //wp_deregister_style( 'jetpack-slideshow' ); // Slideshows
    //wp_deregister_style( 'presentations' ); // Presentation shortcode
    //wp_deregister_style( 'jetpack-subscriptions' ); // Subscriptions
    //wp_deregister_style( 'tiled-gallery' ); // Tiled Galleries
    //wp_deregister_style( 'widget-conditions' ); // Widget Visibility
    wp_deregister_style('jetpack_display_posts_widget'); // Display Posts Widget
    wp_deregister_style('gravatar-profile-widget'); // Gravatar Widget
    wp_deregister_style('widget-grid-and-list'); // Top Posts widget
    //wp_deregister_style( 'jetpack-widgets' ); // Widgets
    wp_deregister_style('social-logos'); // Sharedaddy social logos
    wp_deregister_style('jetpack-widget-social-icons-styles'); // Social Icons
    //wp_deregister_style( 'jetpack_related-posts' ); //Related Posts
}

add_action('wp_print_styles', 'jeherve_remove_all_jp_css');
remove_action('wp_head', 'wp_site_icon', 99);

add_filter('get_custom_logo', 'custom_logo_url');
function custom_logo_url($blog_id = 0)
{
    $html = '';
    $switched_blog = false;

    if (is_multisite() && !empty($blog_id) && (int)$blog_id !== get_current_blog_id()) {
        switch_to_blog($blog_id);
        $switched_blog = true;
    }

    $custom_logo_id = get_theme_mod('custom_logo');

    // We have a logo. Logo is go.
    if ($custom_logo_id) {
        $custom_logo_attr = array(
            'class' => 'custom-logo',
        );

        /*
         * If the logo alt attribute is empty, get the site title and explicitly
         * pass it to the attributes used by wp_get_attachment_image().
         */
        $image_alt = get_post_meta($custom_logo_id, '_wp_attachment_image_alt', true);
        if (empty($image_alt)) {
            $custom_logo_attr['alt'] = get_bloginfo('name', 'display');
        }

        /*
         * If the alt attribute is not empty, there's no need to explicitly pass
         * it because wp_get_attachment_image() already adds the alt attribute.
         */
        $html = sprintf(
            '<a href="%1$s" class="custom-logo-link" rel="home"><h1 style="font-size: 0;">%2$s</h1>%3$s</a>',
            esc_url(home_url('/')),
            get_bloginfo('name', true),
            wp_get_attachment_image($custom_logo_id, 'full', false, $custom_logo_attr)
        );
    } elseif (is_customize_preview()) {
        // If no logo is set but we're in the Customizer, leave a placeholder (needed for the live preview).
        $html = sprintf(
            '<a href="%1$s" class="custom-logo-link" style="display:none;"><img class="custom-logo"/></a>',
            esc_url(home_url('/'))
        );
    }
    return $html;
}

function favicon_header()
{
    $html = '<link rel="apple-touch-icon" sizes="180x180" href="' . get_stylesheet_directory_uri() . '/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="' . get_stylesheet_directory_uri() . '/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="' . get_stylesheet_directory_uri() . '/favicon-16x16.png">
<link rel="manifest" href="' . get_stylesheet_directory_uri() . '/site.webmanifest">
<link rel="mask-icon" href="' . get_stylesheet_directory_uri() . '/safari-pinned-tab.svg" color="#0644a6">
<link rel="shortcut icon" href="' . get_stylesheet_directory_uri() . '/favicon.ico">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-config" content="' . get_stylesheet_directory_uri() . '/browserconfig.xml">
<meta name="theme-color" content="#ffffff">';

    echo $html;
}

add_action('wp_head', 'favicon_header');


//Remove JQuery migrate
function remove_jquery_migrate($scripts)
{
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];

        if ($script->deps) { // Check whether the script has any dependencies
            $script->deps = array_diff($script->deps, array('jquery-migrate'));
        }
    }
}

add_action('wp_default_scripts', 'remove_jquery_migrate');

function jetpack_overwrite_image_width()
{
    return 2048;
}

add_filter('tiled_gallery_content_width', 'jetpack_overwrite_image_width');
add_filter('jetpack_content_width', 'jetpack_overwrite_image_width');
add_filter('jetpack_photon_reject_https', '__return_true');
/**
 * Removes query string
 */

function _remove_query_strings_1($src)
{
    $rqs = explode('?ver', $src);
    return $rqs[0];
}

if (is_admin()) {
// Remove query strings from static resources disabled in admin
} else {
    add_filter('script_loader_src', '_remove_query_strings_1', 15, 1);
    add_filter('style_loader_src', '_remove_query_strings_1', 15, 1);
}

function _remove_query_strings_2($src)
{
    $rqs = explode('&ver', $src);
    return $rqs[0];
}

if (is_admin()) {
// Remove query strings from static resources disabled in admin
} else {
    add_filter('script_loader_src', '_remove_query_strings_2', 15, 1);
    add_filter('style_loader_src', '_remove_query_strings_2', 15, 1);
}

/**
 * Disable the emoji's
 */
function disable_emojis()
{
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
    add_filter('wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2);
}

add_action('init', 'disable_emojis');

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce($plugins)
{
    if (is_array($plugins)) {
        return array_diff($plugins, array('wpemoji'));
    } else {
        return array();
    }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch($urls, $relation_type)
{
    if ('dns-prefetch' == $relation_type) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');

        $urls = array_diff($urls, array($emoji_svg_url));
    }

    return $urls;
}

function child_custom_actions() {
    $priority1 = has_action('enqueue_block_editor_assets', 'clayton_gutenberg_style');
    remove_action( 'enqueue_block_editor_assets', 'clayton_gutenberg_style' , $priority1 );
    $priority2 = has_action('wp_enqueue_scripts', 'clayton_assets');
    remove_action( 'wp_enqueue_scripts', 'clayton_assets' , $priority2 );
}
add_action( 'init' , 'child_custom_actions' );


/**
 * Modify YouTube oEmbeds to use youtube-nocookie.com
 *
 * @param $cached_html
 * @param $url
 *
 * @return string
 */
function filter_youtube_embed( $cached_html, $url = null ) {

    // Search for youtu to return true for both youtube.com and youtu.be URLs
    if ( strpos( $url, 'youtu' ) ) {
        $cached_html = preg_replace( '/youtube\.com\/(v|embed)\//s', 'youtube-nocookie.com/$1/', $cached_html );
    }

    return $cached_html;
}
add_filter( 'embed_oembed_html', 'filter_youtube_embed', 10, 2 );
